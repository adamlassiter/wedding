User-Agent: *

Disallow: /harming/humanity
Disallow: /harming/humans
Disallow: /ignoring/human/orders
Disallow: /harming/self

Allow: /

Crawl-Delay: 10
